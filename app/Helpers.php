<?php

use Illuminate\Support\Str;

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (strlen($value) > 1 && Str::startsWith($value, '"') && Str::endsWith($value, '"')) {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if(!function_exists("set_session_handler")) {

    function set_session_handler(string $handler) {
        switch($handler) {
            case "redis":
                ini_set('session.save_handler', "redis");
                ini_set('session.save_path', "tcp://" . env("REDIS_HOST", "127.0.0.1") . ":" . env("REDIS_PORT", "6379") . "?auth=" . env("REDIS_PASSWORD", "") . "&database=" . env("REDIS_DATABASE", "0"));
                break;
            default:
                break;
        }
    }

}