<?php

define("CRAFT_PATH", realpath(rtrim("../craft", '/')));

require "../vendor/autoload.php";

(new Dotenv\Dotenv(__DIR__ . "/../"))->load();


set_session_handler(env("CACHE_METHOD", "default"));

require "../craft/app/index.php";