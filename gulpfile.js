var elixir = require('laravel-elixir');
var lost = require('lost');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
var favicons = require("gulp-favicons");
var ElixerConfig = elixir.config;

var template_path = './craft/templates';

config('assetsPath', `${template_path}/src`);
config('publicPath', './public/assets');
config('viewPath', template_path);
config('appPath', "./craft");
require(config('assetsPath') + "/gulp/process-css");

elixir(function (mix) {

    mix.processCss(['app.scss'], {
        srcDir: config('assetsPath') + "/sass",
        outputDir: config('publicPath') + "/css",
        plugins: [
            require('postcss-nested'),
            lost(),
            autoprefixer(),
            mqpacker(),
            require('postcss-partial-import')({})
        ]
    });

    mix.browserify('app.js');

    mix.browserSync({
        open: false,
        ghostMode: false,
        proxy: 'http://icartons.dev',
        files: [
            ElixerConfig.appPath + '/**/*.php',
            "!" + ElixerConfig.appPath + '/storage/**/*.php',
            template_path + "/**/*.twig",
            ElixerConfig.get('public.css.outputFolder') + '/**/*.css',
            ElixerConfig.get('public.js.outputFolder') + '/**/*.js',
            ElixerConfig.get('public.versioning.buildFolder') + '/rev-manifest.json'
        ]
    });
    mix.copy(config('assetsPath') + "/images" , config('publicPath') + "/images");
    mix.copy(config('assetsPath') + "/svg" , config('publicPath') + "/svg");
    mix.copy(config('assetsPath') + "/fonts" , config('publicPath') + "/fonts");

    if(config('production')) {
        mix.version(['css/app.css', 'js/app.js']);
    }
});

function config(key, value) {
    if(typeof value != 'undefined') {
        elixir.config[key] = value;
    }
    return elixir.config[key];
}

// Favicons
gulp.task("favicons", function () {
    return gulp.src(config('assetsPath') + "/images/favicon-base.png").pipe(favicons({
            appName: "iCartons",
            appDescription: "iCartons website",
            developerName: "The Concept Store",
            developerURL: "http://theconceptstore.eu/",
            background: "#000000",
            path: "/assets/favicons",
            display: "standalone",
            orientation: "portrait",
            version: 1.0,
            logging: true,
            online: false,
            html: config('viewPath') + "/snippets/_favicons.twig",
            pipeHTML: false,
            replace: true,
            icons: {
                android: false,              // Create Android homescreen icon. `boolean`
                appleIcon: true,            // Create Apple touch icons. `boolean`
                appleStartup: false,         // Create Apple startup images. `boolean`
                coast: false,                // Create Opera Coast icon. `boolean`
                favicons: true,             // Create regular favicons. `boolean`
                firefox: true,              // Create Firefox OS icons. `boolean`
                opengraph: false,            // Create Facebook OpenGraph image. `boolean`
                twitter: false,              // Create Twitter Summary Card image. `boolean`
                windows: true,              // Create Windows 8 tile icons. `boolean`
                yandex: false                // Create Yandex browser icon. `boolean`
            }
        }))
        .pipe(gulp.dest("./public/assets/favicons"));
});

gulp.task('perf-tool', () => {
    let options = {
        siteURL:'https://staging.icartons.eu',
        sitePages: ['/']
    };
    return require('devbridge-perf-tool').performance(options);
});