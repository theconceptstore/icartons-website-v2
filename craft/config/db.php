<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(

	'server' => env("DB_HOST", 'localhost'),
	'database' => env("DB_DATABASE", 'craft'),
	'user' => env("DB_USER", 'root'),
	'password' => env("DB_PASSWORD", ''),
	'tablePrefix' => env("DB_PREFIX", 'craft'),
);
