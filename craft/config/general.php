<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return [
    'devMode' =>env("DEV_MODE", false),
    'usePathInfo' => env("USE_PATH_INFO", true),
    'omitScriptNameInUrls' => env("OMIT_SCRIPT_NAMES_IN_URLS", true),
    'enableTemplateCaching' => env("ENABLE_TEMPLATE_CACHING", false),
    'patrol' => [
        'maintenanceMode'   => env("MAINTENANCE_MODE", true),
    ],
    'environmentVariables' => [
            'basePath' => env("BASE_PATH", $_SERVER['DOCUMENT_ROOT']),
            'baseUrl' => env("BASE_URL", $_SERVER['HTTP_HOST']),
    ],
    'cacheMethod' => env("CACHE_METHOD", 'file'),
    'sendPoweredByHeader' => false,
    'extraAllowedFileExtensions' => 'json',
    'maxUploadFileSize' => 33554432*2,
];

