var gulp = require('gulp');
var elixir = require('laravel-elixir');
var $ = require('gulp-load-plugins')();
var _ = require('underscore');
var cleanCSS = require('gulp-clean-css');

var Task = elixir.Task;

elixir.extend('processCss', function(src, options) {

    var paths = prepGulpPaths(src, options);

    var err = function (e) {
        // line break
        console.log('');
        console.log('PostCSS - Error');
        console.log('   - ' + e.message);
    };

    options = _.extend({
        plugins: []
    }, options);

    new Task('processCss', function() {
        console.log(paths.output.baseDir);

        return gulp.src(paths.src.path)
            .pipe($.if(elixir.config.sourcemaps, $.sourcemaps.init()))
            .pipe($.sass({
                includePaths: [paths.src.baseDir]
            }).on('error', $.sass.logError))
            .pipe($.postcss(options.plugins).on('error', err))
            .pipe($.if(elixir.config.production, cleanCSS()))
            .pipe($.if(elixir.config.sourcemaps, $.sourcemaps.write('.')))
            .pipe(gulp.dest(paths.output.baseDir));
    })
        .watch([
            paths.src.baseDir + '/**/*.+(sass|scss)'
        ]);
});

var prepGulpPaths = function(src, options) {
    return new elixir.GulpPaths()
        .src(src, options.srcDir)
        .output(options.outputDir, options.outputFile);
};