import {lory} from 'lory.js';

export default () => {
    const baseClass = 'Carrousel';
    const slider = document.querySelector(`.${baseClass}`);
    const numberOfslides = document.querySelectorAll(`.${baseClass}--slide`).length;
    let carrousel;

    if(numberOfslides > 1) {

        slider.addEventListener('after.lory.init', function() {
            window.setInterval(() => {
                carrousel.next();
            }, 5000);
        });

        carrousel = lory(slider, {
            classNameFrame: `${baseClass}--frame`,
            classNameSlideContainer: `${baseClass}--slides`,
            classNamePrevCtrl: `${baseClass}--prev`,
            classNameNextCtrl: `${baseClass}--next`,
            rewind: true, rewindSpeed: 300
        });
    }
}