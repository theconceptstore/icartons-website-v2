import CarrouselInit from './Carrousel';
import BarbaInit from './Barba';

document.addEventListener('DOMContentLoaded', () => {
    BarbaInit();
    CarrouselInit();
});
