import Barba from 'barba.js';

export default () => {

    Barba.Pjax.Dom.wrapperId = "b-wrapper";
    Barba.Pjax.Dom.containerClass = "b-container";
    Barba.Pjax.start();
    Barba.Prefetch.init();

}