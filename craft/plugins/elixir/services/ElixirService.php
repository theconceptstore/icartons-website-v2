<?php

namespace Craft;

class ElixirService extends BaseApplicationComponent
{
    /**
     * @var string
     */
    protected $buildPath;

    /**
     * @var string
     */
    protected $publicPath;

    /**
     * ElixirService constructor.
     */
    public function __construct()
    {
        $settings = craft()->plugins->getPlugin('elixir')->getSettings();
        $this->buildPath = $settings->buildPath;
        $this->publicPath = $settings->publicPath;
    }

    /**
     * Find the files version.
     *
     * @param $file
     * @return mixed
     */
    public function version($file)
    {
        $manifest = $this->readManifestFile();

        // if no manifest, return the regular asset
        if (!$manifest) {
            return $file;
        }

        return $this->buildPath . '/' . $manifest[$file];
    }

    /**
     * Returns the assets version with the appropriate tag.
     *
     * @param $file
     * @return string
     */
    public function withTag($file)
    {

        $file = $this->version($file);

        $extension = pathinfo($file, PATHINFO_EXTENSION);

            if ($extension == 'js') {
                return '<script src="' . $file . '"></script>';
            }


        return '<link rel="stylesheet" href="' . $file . '">';
    }

    /**
     * Locate manifest and convert to an array.
     *
     * @return mixed
     */
    protected function readManifestFile()
    {
        $manifestPath = CRAFT_BASE_PATH . $this->publicPath . '/' . $this->buildPath . '/rev-manifest.json';

        if(!file_exists($manifestPath)) {
            return false;
        }

        $manifest = file_get_contents($manifestPath);

        return json_decode($manifest, true);
    }
}