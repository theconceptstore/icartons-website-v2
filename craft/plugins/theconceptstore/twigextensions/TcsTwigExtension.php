<?php
namespace Craft;

use Symfony\Component\Debug\Debug;
use Twig_Extension;

class TcsTwigExtension extends Twig_Extension
{
    /**
     * @var Model
     */
    protected $settings;

    /**
     * TcsTwigExtension constructor.
     * @param $settings
     */
    public function __construct($settings)
    {
        /** @var Model settings */
        $this->settings = $settings;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return "TcsEhv";
    }

    public function getFunctions()
    {
        return array(
            'elixir' => new \Twig_Function_Method($this, 'elixir'),
            'responsiveSet' => new \Twig_Function_Method($this, 'responsiveSet'),
        );
    }

    public function elixir($file)
    {
        static $manifest = null;

        if ((bool)$this->settings->getAttribute('inProduction') === false) {
            return TemplateHelper::getRaw('/assets/' . $file);
        }
        if (is_null($manifest)) {
            $manifest = json_decode(file_get_contents(public_path('assets/build/rev-manifest.json')), true);
        }
        if (isset($manifest[$file])) {
            return TemplateHelper::getRaw('/assets/build/' . $manifest[$file]);
        }
        throw new \InvalidArgumentException("File {$file} not defined in asset manifest.");
    }

    public function responsiveSet($name = 'default')
    {
        /** @var GlobalSetModel $responsiveImageSets */
        $responsiveImageSets = craft()->globals->getSetByHandle('responsiveImageSets');
        $output = [];

        foreach($responsiveImageSets->sets as $set) {
            if($set->setHandle == $name) {
                foreach($set->sizes as $size) {
                    $src = ($size['default'] == true) ? "data-src" : "data-src-" . $size['label'];
                    $output[] = [
                        'width' => (int) $size['size'],
                        'src' => $src,
                    ];
                }
            }
        }
        return $output;
    }
}