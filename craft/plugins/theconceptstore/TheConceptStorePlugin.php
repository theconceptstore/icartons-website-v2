<?php

namespace Craft;


use Guzzle\Service\Client;

class TheConceptStorePlugin extends BasePlugin
{
    public function getName()
    {
        return 'The Concept Store Craft Plugin';
    }

    public function getDescription()
    {
        return 'Base extensions on craft';
    }
    /**
     * Returns the plugin’s version number.
     *
     * @return string The plugin’s version number.
     */
    public function getVersion()
    {
        return '1.0';
    }

    /**
     * Returns the plugin developer’s name.
     *
     * @return string The plugin developer’s name.
     */
    public function getDeveloper()
    {
        return 'The Concept Store';
    }

    /**
     * Returns the plugin developer’s URL.
     *
     * @return string The plugin developer’s URL.
     */
    public function getDeveloperUrl()
    {
        return 'https://theconceptstore.nl';
    }
    public function init()
    {
        require_once __DIR__ . '/vendor/autoload.php';
    }

    public function addTwigExtension()
    {
        Craft::import('plugins.theconceptstore.twigextensions.TcsTwigExtension');
        return new TcsTwigExtension($this->getSettings());
    }

    protected function defineSettings()
    {
        return array(
            'inProduction' => array(AttributeType::Bool)
        );
    }

    public function getSettingsHtml()
    {
        return craft()->templates->render('theconceptstore/settings', array(
            'settings' => $this->getSettings()
        ));
    }
}